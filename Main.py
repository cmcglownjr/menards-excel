#!/usr/bin/env python3

"""
Python program used to populate the Menards New Product Form with Porch Conversions product information.
"""

import openpyxl
import csv

# Lets define some files
input_wb = "New Product Form.xlsx"
output_wb = "PorchConversions_Product_list.xlsx"
image_csv = open('Products.csv', 'w', newline='')
fieldnames = ['Filename', 'Description']
writer = csv.DictWriter(image_csv, fieldnames=fieldnames)
writer.writeheader()
# Functions and variables used in setting product IDs for the windows
# Create dictionaries for the windows
window_frame_color = {'WH': 'White', 'TN': 'Tan', 'CL': 'Clay', 'BZ': 'Bronze', 'BK': 'Black'}
window_vinyl_color = {'CLR': 'Clear', 'LGR': 'Light Grey', 'DGR': 'Dark Grey', 'BRZ': 'Bronze'}
window_screen_type = {'NS': 'No Screen', 'SD': 'Standard', 'BV': 'Better View', 'SS': 'Super Screen'}
window_type = {1: '3V', 0: '4V'}  # 3 vent = 1, 4 vent = 0
window_type2 = {1: '3 Vent', 0: '4 Vent'}
# The prices aren't in a set formula so I have to type each out...
# 3 or 4 vent windows. 3 vent = 1, 4 vent = 0
window_price_height = {30: [[204.0, 1], [214.0, 1], [229.0, 1], [239.0, 1], [256.0, 1], [269.0, 1], [282.0, 1]],
                       36: [[214.0, 0], [229.0, 1], [239.0, 1], [256.0, 1], [269.0, 1], [282.0, 1], [295.0, 1]],
                       42: [[229.0, 0], [239.0, 1], [256.0, 1], [269.0, 1], [282.0, 1], [295.0, 1], [316.0, 1]],
                       48: [[239.0, 0], [256.0, 1], [269.0, 1], [282.0, 1], [295.0, 1], [316.0, 1], [330.0, 1]],
                       54: [[256.0, 0], [269.0, 0], [282.0, 0], [295.0, 1], [316.0, 1], [330.0, 1], [346.0, 1]],
                       60: [[269.0, 0], [282.0, 0], [295.0, 0], [316.0, 0], [330.0, 0], [346.0, 0], [354.0, 0]],
                       66: [[282.0, 0], [295.0, 0], [316.0, 0], [330.0, 0], [346.0, 0], [354.0, 0], [380.0, 0]],
                       72: [[295.0, 0], [316.0, 0], [330.0, 0], [346.0, 0], [354.0, 0], [380.0, 0], [393.0, 0]],
                       78: [[316.0, 0], [330.0, 0], [346.0, 0], [354.0, 0], [380.0, 0], [393.0, 0], [415.0, 0]],
                       84: [[330.0, 0], [346.0, 0], [354.0, 0], [380.0, 0], [393.0, 0], [415.0, 0], [418.0, 0]],
                       90: [[334.0, 0], [354.0, 0], [380.0, 0], [393.0, 0], [415.0, 0], [418.0, 0], [436.0, 0]],
                       96: [[354.0, 0], [380.0, 0], [393.0, 0], [415.0, 0], [418.0, 0], [436.0, 0], [445.0, 0]],
                       102: [[380.0, 0], [393.0, 0], [415.0, 0], [418.0, 0], [436.0, 0]]}

window_vent_width = [24, 30, 36, 42, 48, 54, 60]
lip_frame_window_type1 = {'FGV': 'FlexiGlaze Vinyl', 'TG': 'Tempered Glass'}
lip_frame_window_type2 = {'SD': 'Standard', 'BV': 'Better View', 'SS': 'Super Screen'}


# one function for calculating options and deductions with screen types
def screen_options(screenwidth, screenheight, base_price, screen_type):
    if screen_type == 'BV':
        constant = 0.25
    elif screen_type == 'SS':
        constant = 0.40
    elif screen_type == 'NS':
        constant = -0.28
    else:
        constant = 0
    return base_price + (screenheight + screenwidth) * constant


def lip_frame_price(screenwidth, screenheight, glasstype):
    ui = screenwidth + screenheight
    if glasstype == 'FGV':
        if ui <= 70:
            price = 85.0
        else:
            price = 1.4 * ui
    elif glasstype == 'TG':
        if ui <= 70:
            price = 295.0
        else:
            price = 4.5 * ui
    elif glasstype == 'SD':
        if ui <= 70:
            price = 75.0
        else:
            price = 1.25*ui
    elif glasstype == 'BV':
        if ui <= 70:
            price = 85.0
        else:
            price = 1.4*ui
    elif glasstype == 'SS':
        if ui <= 70:
            price = 85.0
        else:
            price = 1.4 * ui
    return price


def common_excel_fills():
    active_sheet['A{}'.format(row)].value = 'mparks@menard-inc.com'
    active_sheet['F{}'.format(row)].value = ''
    active_sheet['G{}'.format(row)].value = ''
    active_sheet['H{}'.format(row)].value = 'EACH'
    active_sheet['I{}'.format(row)].value = 'Net 30'
    active_sheet['J{}'.format(row)].value = 28
    active_sheet['O{}'.format(row)].value = 'MENARD ARRANGED'
    active_sheet['P{}'.format(row)].value = 'PALLETIZED'
    active_sheet['Q{}'.format(row)].value = 'N'
    active_sheet['W{}'.format(row)].value = 'One Unit Per Order'
    active_sheet['X{}'.format(row)].value = 'Y'
    active_sheet['Y{}'.format(row)].value = 'United States (US)'
    active_sheet['Z{}'.format(row)].value = 'Made in the USA'
    active_sheet['AA{}'.format(row)].value = 'Maumee'
    active_sheet['AB{}'.format(row)].value = 'OH'
    active_sheet['AC{}'.format(row)].value = 'Eagle View Porch Windows'
    active_sheet['AG{}'.format(row)].value = 1
    active_sheet['AH{}'.format(row)].value = height
    active_sheet['AI{}'.format(row)].value = width
    # active_sheet['AJ{}'.format(row)].value
    # active_sheet['AK{}'.format(row)].value
    active_sheet['AL{}'.format(row)].value = 1
    active_sheet['AM{}'.format(row)].value = 'As is product-No Package'
    active_sheet['AN{}'.format(row)].value = 'No'
    active_sheet['AO{}'.format(row)].value = 'No'
    active_sheet['BT{}'.format(row)].value = 'N'
    active_sheet['BU{}'.format(row)].value = 'N'
    active_sheet['BV{}'.format(row)].value = 'N'
    active_sheet['BX{}'.format(row)].value = 'N'
    active_sheet['BZ{}'.format(row)].value = 'N'
    active_sheet['CA{}'.format(row)].value = 'N'
    active_sheet['CB{}'.format(row)].value = 'N'
    active_sheet['CC{}'.format(row)].value = 'N'
    active_sheet['CD{}'.format(row)].value = 'No'
    active_sheet['CE{}'.format(row)].value = 'Not Labeled'
    active_sheet['CF{}'.format(row)].value = 'NOT REQUIRED'
    active_sheet['CH{}'.format(row)].value = 'N'
    active_sheet['CI{}'.format(row)].value = 'N'
    active_sheet['CJ{}'.format(row)].value = 24


# Functions and variables used in setting product IDs for the doors
door_widths = [32, 34, 36]
door_height = 80
door_base_price = 818.00
door_frame_color = {'WH': 'white', 'TN': 'tan', 'CL': 'clay', 'BZ': 'bronze', 'BK': 'black'}
door_options = {'threshold': 62.00, 'up-charge': 39.00, 'French Door kit': 73.00, 'ExtraCloser': 65.00,
                'Better View Screen': 35.00, 'Super Screen': 35.00}
door_warranty = 2  # 2 years warranty

row = 4
window_width = 24
window_height = 30
count = 0
try:
    wb = openpyxl.load_workbook(input_wb)
    active_sheet = wb['Sheet1']
    # Start the FOR loops for the windows
    for i in window_frame_color:
        for j in window_vinyl_color:
            for height in window_price_height:
                for ll in range(len(window_price_height[height])):
                    for k in window_screen_type:
                        width = window_vent_width[ll]
                        # print('Window height is: {}'.format(height))
                        # print('Window width is: {}'.format(width))
                        image_filename_3V = i + '_' + j + '_' + '3V.jpg'
                        image_filename_3V_description = '3 Vent Window Frame Color: {}, Vinyl Color: {}, ' \
                                                        'Window Type: 3-Vent' \
                            .format(window_frame_color[i], window_vinyl_color[j])
                        image_filename_4V = i + '_' + j + '_' + '4V.jpg'
                        image_filename_4V_description = '4 Vent Window Frame Color: {}, Vinyl Color: {}, ' \
                                                        'Window Type: 4-Vent' \
                            .format(window_frame_color[i], window_vinyl_color[j])
                        # Fill in Menards New Product Form
                        active_sheet['B{}'.format(row)].value = i + '_' + j + '_' + \
                                                                window_type[window_price_height[height][ll][1]] + '_' + \
                                                                k + str(width) + 'X' + str(height)
                        active_sheet['C{}'.format(row)].value = "Frame Color: {}, Vinyl Color: {}, Window Type: {}, " \
                                                                "Screen Type: {}, Width (in): {}, Height (in): {}" \
                            .format(window_frame_color[i], window_vinyl_color[j],
                                    window_type2[window_price_height[height][ll][1]], window_screen_type[k], str(width),
                                    str(height))
                        active_sheet['E{}'.format(row)].value = "{:0,.2f}" \
                            .format(screen_options(width, height, window_price_height[height][ll][0], k))
                        common_excel_fills()
                        active_sheet['CK{}'.format(row)].value = i + '_' + j + '_' + \
                                                                 window_type[window_price_height[height][ll][1]] + \
                                                                 '.jpg'
                        row += 1
                        count += 1
            writer.writerow({'Filename': image_filename_3V, 'Description': image_filename_3V_description})
            writer.writerow({'Filename': image_filename_4V, 'Description': image_filename_4V_description})
    # Lip Frame FlexiGlaze or Tempered Glass
    window_count = count
    count = 0
    for i in window_frame_color:
        for j in range(6, 52+1, 4):
            for k in range(6, 96+1, 4):
                image_filename = i + '_LipFrame_' + 'FGV' + '.jpg'
                image_filename_description = 'Lip Frame Window Frame Color: {}, Glass Type: {}'\
                    .format(window_frame_color[i], lip_frame_window_type1['FGV'])
                active_sheet['B{}'.format(row)].value = i + '_LipFrame_' + 'FGV' + '_' + str(j) + 'X' + str(k)
                active_sheet['C{}'.format(row)].value = "Frame Color: {}, Glass Type: {}, Width (in): {}, " \
                                                        "Height (in): {}"\
                    .format(window_frame_color[i], lip_frame_window_type1['FGV'], str(j), str(k))
                active_sheet['E{}'.format(row)].value = "{:0,.2f}".format(lip_frame_price(j, k, 'FGV'))
                common_excel_fills()
                active_sheet['CK{}'.format(row)].value = image_filename
                row += 1
                count += 1
        writer.writerow({'Filename': image_filename, 'Description': image_filename_description})
    lipframe_window_count1 = count
    count = 0
    for i in window_frame_color:
        for j in range(12, 42+1, 2):
            for k in range(12, 96+1, 2):
                image_filename = i + '_LipFrame_' + 'TG' + '.jpg'
                image_filename_description = 'Lip Frame Window Frame Color: {}, Glass Type: {}'\
                    .format(window_frame_color[i], lip_frame_window_type1['TG'])
                active_sheet['B{}'.format(row)].value = i + '_LipFrame_' + 'TG' + '_' + str(j) + 'X' + str(k)
                active_sheet['C{}'.format(row)].value = "Frame Color: {}, Glass Type: {}, Width (in): {}, " \
                                                        "Height (in): {}"\
                    .format(window_frame_color[i], lip_frame_window_type1['TG'], str(j), str(k))
                active_sheet['E{}'.format(row)].value = "{:0,.2f}".format(lip_frame_price(j, k, 'TG'))
                common_excel_fills()
                active_sheet['CK{}'.format(row)].value = image_filename
                row += 1
                count += 1
        writer.writerow({'Filename': image_filename, 'Description': image_filename_description})
    lipframe_window_count2 = count
    count = 0
    for i in window_frame_color:
        for ll in lip_frame_window_type2:
            for j in range(6, 60+1, 6):
                for k in range(6, 96+1, 6):
                    image_filename = i + '_LipFrame_' + ll + '.jpg'
                    image_filename_description = 'Lip Frame Window Frame Color: {}, Glass Type: {}' \
                        .format(window_frame_color[i], lip_frame_window_type2[ll])
                    active_sheet['B{}'.format(row)].value = i + '_LipFrame_' + ll + '_' + str(j) + 'X' + str(k)
                    active_sheet['C{}'.format(row)].value = "Frame Color: {}, Glass Type: {}, Width (in): {}, " \
                                                            "Height (in): {}" \
                        .format(window_frame_color[i], lip_frame_window_type2[ll], str(j), str(k))
                    active_sheet['E{}'.format(row)].value = "{:0,.2f}".format(lip_frame_price(j, k, ll))
                    common_excel_fills()
                    active_sheet['CK{}'.format(row)].value = image_filename
                    row += 1
                    count += 1
            writer.writerow({'Filename': image_filename, 'Description': image_filename_description})
    lipframe_window_count3 = count
    count = 0
    for i in door_frame_color:
        for j in door_widths:
            image_filename = i + '_' + 'cabanadoor.jpg'
            image_filename_description = 'Cabana Door Frame Color: {}'.format(door_frame_color[i])
            active_sheet['B{}'.format(row)].value = i + '_cabanadoor_' + str(j) + 'X' + str(door_height)
            active_sheet['C{}'.format(row)].value = "Frame Color: {}, Door Width (in): {}, Door Height (in): {}"\
                .format(door_frame_color[i], j, door_height)
            active_sheet['E{}'.format(row)].value = "{:0,.2f}".format(door_base_price)
            common_excel_fills()
            active_sheet['CK{}'.format(row)].value = image_filename
            row += 1
            count += 1
        writer.writerow({'Filename': image_filename, 'Description': image_filename_description})
    door_count = count
    count = 0
finally:
    wb.save(output_wb)
    image_csv.close()
    wb.close()
print(window_count)
print(lipframe_window_count1)
print(lipframe_window_count2)
print(lipframe_window_count3)
print(door_count)
print('Total: {}'.format(lipframe_window_count1 + lipframe_window_count2 + lipframe_window_count3 + window_count +
                         door_count))